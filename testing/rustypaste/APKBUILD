# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=rustypaste
pkgver=0.9.1
pkgrel=0
pkgdesc="Minimal file upload/pastebin service"
url="https://github.com/orhun/rustypaste"
# s390x, ppc64le, riscv64: blocked by ring crate
arch="all !s390x !ppc64le !riscv64"
license="MIT"
makedepends="cargo zstd-dev openssl-dev cargo-auditable"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/orhun/rustypaste/archive/v$pkgver.tar.gz"
options="net"

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libzstd.
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		zstd = { rustc-link-lib = ["zstd"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release --no-default-features --features openssl
}

check() {
	cargo test --frozen -- --test-threads 1
}

package() {
	install -Dm 755 "target/release/$pkgname" -t "$pkgdir/usr/bin"
	install -Dm 644 config.toml -t "$pkgdir/etc/rustypaste"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}

sha512sums="
1c91bcc3d9bd42aca6980a1cd7a51d2772f6ce6775d49bbe8e3e46e2eea3560392ebe1c4404b21cfb556976695fc56e33f231ac304c347a5294ee37ffb9aa2cc  rustypaste-0.9.1.tar.gz
"

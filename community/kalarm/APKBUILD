# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kalarm
pkgver=23.04.1
pkgrel=1
pkgdesc="Personal alarm scheduler"
url="https://kontact.kde.org/"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="kdepim-runtime"
makedepends="
	akonadi-contacts-dev
	akonadi-dev
	akonadi-mime-dev
	extra-cmake-modules
	kauth-dev
	kcalendarcore-dev
	kcalutils-dev
	kcmutils-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	kdelibs4support-dev
	kdoctools-dev
	kglobalaccel-dev
	kguiaddons-dev
	kholidays-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kidletime-dev
	kimap-dev
	kio-dev
	kjobwidgets-dev
	kmailtransport-dev
	kmime-dev
	knotifications-dev
	knotifyconfig-dev
	kpimtextedit-dev
	kservice-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkdepim-dev
	libxslt-dev
	mailcommon-dev
	phonon-dev
	pimcommon-dev
	qgpgme
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalarm-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Tests are broken

build() {
	cmake -B build -G Ninja\
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9f25c9b558c79f8ac4c8cf0f88aba82c2cba3e2c21e6a5c8f99cb074b7dc6b7ebd61ccadc5f613c35ca560330c7016b74f37104f26916bcbd6e926a4d1247028  kalarm-23.04.1.tar.xz
"

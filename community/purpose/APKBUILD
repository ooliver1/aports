# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=purpose
pkgver=5.106.0
pkgrel=0
pkgdesc="Framework for providing abstractions to get the developer's purposes fulfilled"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> kaccounts-integration
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kaccounts-integration-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
options="!check" # fails on builders
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/purpose-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=ON
	cmake --build build
}

check() {
	cd build
	# This is 1 of 2 tests
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E 'menutest'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4ed8f6b4fd5a2e4fff02380aded436a2ffaadde9a97e48c6430800db597e1fd46dd5aba3b57888d62838a9d316e2ac6e0b0acc4878c723e29f0231d5a692c33e  purpose-5.106.0.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kleopatra
pkgver=23.04.1
pkgrel=0
# riscv64 disabled due to missing rust in recursive dependency
arch="all !armhf !s390x !riscv64"
url="https://www.kde.org/applications/utilities/kleopatra/"
pkgdesc="Certificate Manager and Unified Crypto GUI"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="
	gnupg
	pinentry-qt
	"
makedepends="
	boost-dev
	extra-cmake-modules
	gpgme-dev
	kcmutils-dev
	kcodecs-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kitemmodels-dev
	kmime-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libassuan-dev
	libkleo-dev
	qgpgme
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kleopatra-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kuniqueservicetest requires running dbus
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kuniqueservicetest"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
82f0cf62a47aa2402f47b7df84c021d4dd83e542eb3a3461e58895572e685ab29deeb6344cf18b782ed48dd1dd1d9552c1e74176c2937f2ea8c7683eff4b5f0b  kleopatra-23.04.1.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze-icons
pkgver=5.106.0
pkgrel=0
pkgdesc="Breeze icon themes"
arch="noarch !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-3.0-or-later"
makedepends="
	extra-cmake-modules
	py3-lxml
	python3
	qt5-qtbase-dev
	samurai
	"
checkdepends="bash"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/breeze-icons-$pkgver.tar.xz"

# Several KDE applications use icons not yet present in most themes
# We want to keep the possibility for users to not use the KDE provided
# breeze-icons theme however, as hopefully in the future this situation changes
# Thus let any theme that provides these icons provide "kde-icons" so the user
# retains their ability to choose their preferred theme
provides="kde-icons"
provider_priority=100

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBINARY_ICONS_RESOURCE=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(dupe|symlink)'
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
ff4a7e40872f6dadee5d4d8190813b5246cb79f3ca3a0af156db258fb2e3ef94042a26e93ccaaa53c9372c512317629316eff10e0292ba0127ed9f354ab47c79  breeze-icons-5.106.0.tar.xz
"

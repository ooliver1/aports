# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=mailimporter
pkgver=23.04.1
pkgrel=0
pkgdesc="KDE PIM library providing support for mail applications"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-or-later"
depends_dev="
	akonadi-dev
	akonadi-mime-dev
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kmime-dev
	libkdepim-dev
	pimcommon-dev
	qt5-qtbase-dev
	"
makedepends="
	$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/mailimporter-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
775d185a61c2e99ffc8baf4bdf959532b2601eab579dc15d05619303892996e85e51ed0dc359b42bc8ecb7e65113026ac5f8ac6dc379edf8e633d3948a34490e  mailimporter-23.04.1.tar.xz
"

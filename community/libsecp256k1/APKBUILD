# Maintainer: Michał Adamski <michal@ert.pl>
pkgname=libsecp256k1
pkgver=0.3.1
pkgrel=1
pkgdesc="Optimized C library for EC operations on curve secp256k1"
url="https://github.com/bitcoin-core/secp256k1"
arch="all"
license="MIT"
makedepends="autoconf automake libtool"
subpackages="$pkgname-dev"
source="https://github.com/bitcoin-core/secp256k1/archive/v$pkgver/secp256k1-v$pkgver.tar.gz"
builddir="$srcdir/secp256k1-$pkgver"

prepare() {
	default_prepare

	./autogen.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-static \
		--enable-module-ecdh \
		--enable-module-recovery \
		--disable-benchmark \
		--disable-exhaustive-tests \
		--disable-examples
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
58ec679625ef4663c94c7198ee953283106c1e027623c3de341277fa757265b822ad3c0e98019f9e425f0e9e414b03e6b62853a1581611813cce753e81845d0f  secp256k1-v0.3.1.tar.gz
"

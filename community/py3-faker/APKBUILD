# Contributor: Drew DeVault <sir@cmpwn.com>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-faker
_pyname=Faker
pkgver=18.9.0
pkgrel=0
pkgdesc="Python package that generates fake data for you"
url="https://faker.readthedocs.io/en/master"
license="MIT"
arch="noarch"
depends="py3-dateutil"
makedepends="py3-setuptools"
checkdepends="py3-email-validator py3-ipaddress py3-mock py3-freezegun
	py3-more-itertools py3-pytest py3-ukpostcodeparser py3-validators
	py3-pytest-runner py3-random2 py3-pillow"
_pypiprefix="${_pyname%${_pyname#?}}"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/$_pypiprefix/$_pyname/$_pyname-$pkgver.tar.gz"
builddir=$srcdir/$_pyname-$pkgver

replaces="py-faker" # Backwards compatibility
provides="py-faker=$pkgver-r$pkgrel" # Backwards compatibility

case "$CARCH" in
	# blocked by py3-pillow
	s390x|riscv64) options="!check" ;;
esac

prepare() {
	default_prepare
	# tests erroneously require a specific version of pytest
	sed -i setup.py -e 's/ *"pytest>=.*//g'
}

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$PWD" pytest
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
9ad60cc30121405fdac38ef479ca1f8bf9114a599575caeded5be4fb8fed80a3268ad02705217ab0ce006e017d2aae7be3c1b50cacddc49bc16cdeb9bc9347c9  Faker-18.9.0.tar.gz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kaddressbook
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://apps.kde.org/kaddressbook/"
pkgdesc="Address Book application to manage your contacts"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends="kdepim-runtime"
makedepends="
	akonadi-dev
	akonadi-search-dev
	extra-cmake-modules
	gpgme-dev
	grantleetheme-dev
	kcmutils-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kontactinterface-dev
	kpimtextedit-dev
	kuserfeedback-dev
	kuserfeedback-dev
	libkdepim-dev
	libkleo-dev
	pimcommon-dev
	prison-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaddressbook-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja\
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8675019c4c4c140e62c79cbef01facea44d0a2b0712ba1c0d689130fa8bf332c58f91d129a99c82d9c59f7af9bb815f742d02974b919f0dac082e9e15c555817  kaddressbook-23.04.1.tar.xz
"

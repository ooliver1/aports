# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ark
pkgver=23.04.1
pkgrel=0
pkgdesc="Graphical file compression/decompression utility with support for multiple formats"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.ark"
license="GPL-2.0-only"
depends="
	7zip-virtual
	lrzip
	unzip
	zip
	zstd
	"
makedepends="
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	kparts-dev
	kpty-dev
	kservice-dev
	kwidgetsaddons-dev
	libarchive-dev
	libzip-dev
	qt5-qtbase-dev
	samurai
	shared-mime-info
	xz-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/ark-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

# secfixes:
#   20.08.0-r1:
#     - CVE-2020-24654
#   20.04.3-r1:
#     - CVE-2020-16116

case "$CARCH" in
	# FIXME: tests fail, see alpine/aports#14266
	x86_64) options="!check";;
esac

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run -a ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
2b17a50901164a4e92f218247818266c72a7a4f909b3a28e21bb87455bebc42fc83097392f8e7346d19d870e2d19e5f8b184c0016034509956e32722397418ae  ark-23.04.1.tar.xz
"

# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=font-iosevka
pkgver=24.1.0
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/iosevka-aile.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
2b52976abdb4d7a62989e3545fff36afdf1891480cd2cc3020b1cdb56297bc1834cf12e4af5fe23b32cd90847993e46de5f8b16f9fa2392553776622c1932cdb  super-ttc-iosevka-24.1.0.zip
20549d513c4dcae2911eef2a362f675b592d4cac5b5b981e581287e482c16e32e8e9ad00f694b96ca714f623b2c6b4dd51ea47632bcc65867c52450cbbfa6b77  super-ttc-iosevka-aile-24.1.0.zip
5bd131ed1b034000384d54560b50c65dddc2cf69695c6ec21bb52be10a4b1d87d1ddea2733acc02dd142ff185a257412e368ede2f5cda5b8de4f91c4e8d3e47c  super-ttc-iosevka-slab-24.1.0.zip
986dec8f40114955020f8ccf1b468ad155ca9180c4658d4bfaabdc1e80065a4b1e41beeddb4a6e3a0b320aeb4abd365a973099572fd57343be398d95c6418ad0  super-ttc-iosevka-curly-24.1.0.zip
442ee7a6a132220a9dcf8a67e5002eed3e8761fa0114c10b1af5d1c7dead4504f2fa3adacb61d9850786ff7f599774286cf1793f45aa138e72e0ccb5adf6cea4  super-ttc-iosevka-curly-slab-24.1.0.zip
"

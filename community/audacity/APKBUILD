# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=audacity
pkgver=3.3.2
pkgrel=1
pkgdesc="Multitrack audio editor"
url="https://www.audacityteam.org/"
# s390x: fails to build
arch="all !s390x"
license="GPL-3.0-or-later"
makedepends="
	alsa-lib-dev
	cmake
	expat-dev
	ffmpeg-dev
	flac-dev
	jack-dev
	lame-dev
	libid3tag-dev
	libmad-dev
	libogg-dev
	libsndfile-dev
	libvorbis-dev
	lilv-dev
	lv2-dev
	mpg123-dev
	nasm
	portaudio-dev
	portmidi-dev
	samurai
	soundtouch-dev
	soxr-dev
	sqlite-dev
	suil-dev
	taglib-dev
	vamp-sdk-dev
	wavpack-dev
	wxwidgets-dev
	zlib-dev
	"
subpackages="$pkgname-doc $pkgname-lang"
source="https://github.com/audacity/audacity/releases/download/Audacity-$pkgver/audacity-sources-$pkgver.tar.gz"
# no tests
options="!check"
ldpath="/usr/lib/audacity"

provides="tenacity=$pkgver-r$pkgrel"
replaces="tenacity"

builddir="$srcdir"/audacity-sources-$pkgver

prepare() {
	default_prepare

	# hide aports version
	git init -q .
}

build() {
	case "$CARCH" in
	x86)
		local arch="-DHAVE_SSE=OFF -DHAVE_SSE2=OFF -DHAVE_MMX=OFF"
		;;
	x86_64)
		local arch="-DHAVE_SSE=ON -DHAVE_SSE2=ON -DHAVE_MMX=ON"
		;;
	esac

	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi

	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DAUDACITY_BUILD_LEVEL=2 \
		-Daudacity_conan_enabled=OFF \
		-Daudacity_has_vst3=OFF \
		-Daudacity_has_crashreports=OFF \
		-Daudacity_has_networking=OFF \
		-Daudacity_has_sentry_reporting=OFF \
		-Daudacity_has_updates_check=OFF \
		-Daudacity_lib_preference=system \
		-Daudacity_obey_system_dependencies=ON \
		-Daudacity_use_portsmf=local \
		-Daudacity_use_sbsms=local \
		-Daudacity_use_twolame=local \
		$CMAKE_CROSSOPTS \
		$arch

	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8b6214289b2aa2a9524d895ffd782ac1a55f31ac03da1bf8f52dc35ccf1ea46e2f27bb3b4e01ec20b039ea670aed9e2af55949d636e5c8b0168cab3f16877aeb  audacity-sources-3.3.2.tar.gz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=frameworkintegration
pkgver=5.106.0
pkgrel=0
pkgdesc="Framework providing components to allow applications to integrate with a KDE Workspace"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org"
license="LGPL-2.1-or-later"
depends_dev="
	appstream-dev
	kconfig-dev
	kconfigwidgets-dev
	ki18n-dev
	kiconthemes-dev
	knewstuff-dev
	knotifications-dev
	kpackage-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/frameworkintegration-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bf0baafcdda7ecd9f66501a1585af3c6bc8417c17168ee11883bfe9096bb00f0a93313c246296bfdc096c230ff35d4f3fc223da7cba875aca2978b42b6bb5cb5  frameworkintegration-5.106.0.tar.xz
"

# Contributor: Alex Yam <alex@alexyam.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=imath
pkgver=3.1.8
pkgrel=0
pkgdesc="C++ and python library of 2D and 3D vector, matrix, and math operations for computer graphics"
url="https://github.com/AcademySoftwareFoundation/Imath"
arch="all"
license="BSD-3-Clause"
makedepends="
	bash
	boost-dev
	clang-extra-tools
	cmake
	doxygen
	py3-numpy-dev
	python3-dev
	samurai
	"
subpackages="$pkgname-dev py3-$pkgname:_py"
source="$pkgname-$pkgver.tar.gz::https://github.com/AcademySoftwareFoundation/Imath/archive/refs/tags/v$pkgver.tar.gz"
builddir=$srcdir/Imath-$pkgver

# openexr used to vendor an imath that was system installed
replaces="openexr"

case "$CARCH" in
x86)
	options="$options !check"
	# fails a bunch of tests
	;;
esac

build() {
	CFLAGS="$CFLAGS -O2 -flto=auto" \
	CXXFLAGS="$CXXFLAGS -O2 -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DPYTHON=ON
	cmake --build build
}

check() {
	cd build && CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

_py() {
	pkgdesc="Imath Python library"
	depends="python3 $pkgname=$pkgver-r$pkgrel"
	amove usr/lib/python3*
}

sha512sums="
c71f040956284bf1fbb711593f23219f7b21ac8bfe7552f4b4c75312e376f7940b5bb0a816455967e3ea36fd548059b2d7621e0e11f33e273328fdb557eccaf9  imath-3.1.8.tar.gz
"
